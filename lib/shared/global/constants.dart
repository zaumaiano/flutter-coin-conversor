import 'package:flutter_coin_conversor/domain/model/location.dart';

class Constants {
  static List<CountryModel> countries = [
    CountryModel(
      code: 'AKZ',
      name: 'Angola',
      locale: 'pt_PT',
      symbol: 'AKZ',
      flag: 'assets/images/flags/flag-angola.png',
    ),
    CountryModel(
        code: 'BRL',
        name: 'Brasil',
        locale: 'pt_BR',
        symbol: r'R$',
        flag: 'assets/images/flags/flag-brasil.png'),
    CountryModel(
        code: 'USD',
        name: 'Estados Unidos da América',
        locale: 'en_US',
        symbol: r'$ ',
        flag: 'assets/images/flags/flag-usa.png'),
  ];
}
