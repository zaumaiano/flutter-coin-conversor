import 'package:intl/intl.dart';
import 'package:flutter_coin_conversor/domain/model/location.dart';

buildNumberFormat(CountryModel country, var value) {
  return NumberFormat.currency(
    locale: country.locale,
    symbol: country.symbol,
  ).format(value).toString();
}
