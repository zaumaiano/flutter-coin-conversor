import 'package:flutter/material.dart';
import 'package:flutter_coin_conversor/ui/page/home.dart';

class CoinConversorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Coin Conversor',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(title: 'Coin Conversor'),
    );
  }
}
