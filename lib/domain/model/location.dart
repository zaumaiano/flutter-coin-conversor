class CountryModel {
  final String code;
  final String name;
  final String locale;
  final String symbol;
  final String flag;

  CountryModel({
    this.code,
    this.name,
    this.locale,
    this.symbol,
    this.flag,
  });
}
