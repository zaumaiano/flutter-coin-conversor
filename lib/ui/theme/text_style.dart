import 'package:flutter/material.dart';

TextStyle buildTextStyle({bool isBold}) => isBold
    ? TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
      )
    : TextStyle(
        fontSize: 20,
      );
