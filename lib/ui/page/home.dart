import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_coin_conversor/ui/theme/text_style.dart';
import 'package:flutter_coin_conversor/domain/model/location.dart';
import 'package:flutter_coin_conversor/ui/widget/alert_widget.dart';
import 'package:flutter_coin_conversor/shared/global/constants.dart';
import 'package:flutter_coin_conversor/ui/widget/result_widget.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _formKey = GlobalKey<FormState>();

  CountryModel selectedCountry;
  var currentValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          autovalidateMode: AutovalidateMode.always,
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Formatar Moeda',
                  style: buildTextStyle(isBold: true),
                ),
                Divider(),
                SizedBox(height: 30),
                TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                  ],
                  decoration: InputDecoration(
                    labelText: 'Informe o Valor',
                    labelStyle: buildTextStyle(isBold: false),
                    hintText: 'Valor',
                    hintStyle: buildTextStyle(isBold: false),
                    filled: true,
                    border: UnderlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Digite um valor válido!';
                    }
                    return null;
                  },
                  onChanged: (changedValue) {
                    setState(() {
                      currentValue = changedValue;
                    });
                  },
                ),
                SizedBox(height: 30),
                DropdownButtonFormField<CountryModel>(
                  isExpanded: true,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    filled: true,
                    labelText: 'Selecione um país',
                    labelStyle: buildTextStyle(isBold: false),
                  ),
                  value: selectedCountry,
                  items: Constants.countries.map((CountryModel country) {
                    return DropdownMenuItem(
                      value: country,
                      child: Row(
                        children: [
                          Image.asset(
                            country.flag,
                            height: 20,
                          ),
                          SizedBox(width: 10),
                          Text(
                            country.name,
                            style: buildTextStyle(isBold: false),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                  onChanged: (currentLocation) {
                    setState(() {
                      selectedCountry = currentLocation;
                    });
                  },
                ),
                SizedBox(height: 120),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    elevation: 6,
                    child: Padding(
                      padding: const EdgeInsets.all(50.0),
                      child: selectedCountry == null
                          ? AlertWidget(message: 'Selecione um país!')
                          : _formKey.currentState.validate()
                              ? ResultWidget(
                                  country: selectedCountry,
                                  value: currentValue,
                                )
                              : AlertWidget(
                                  message: 'Digite um valor não é válido!'),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _formKey.currentState.reset();
            selectedCountry = null;
          });
        },
        tooltip: 'Limpar',
        child: Icon(Icons.cleaning_services_outlined),
      ),
    );
  }
}
