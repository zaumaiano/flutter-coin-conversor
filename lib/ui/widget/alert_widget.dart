import 'package:flutter/material.dart';
import 'package:flutter_coin_conversor/ui/theme/text_style.dart';

class AlertWidget extends StatelessWidget {
  final String message;

  const AlertWidget({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.warning,
          color: Colors.orange,
          size: 35,
        ),
        SizedBox(width: 20),
        Text(
          message,
          style: buildTextStyle(isBold: false),
        )
      ],
    );
  }
}
