import 'package:flutter/material.dart';
import 'package:flutter_coin_conversor/ui/theme/text_style.dart';
import 'package:flutter_coin_conversor/domain/model/location.dart';
import 'package:flutter_coin_conversor/shared/format/currency.dart';

class ResultWidget extends StatelessWidget {
  final CountryModel country;
  final dynamic value;

  const ResultWidget({Key key, @required this.country, @required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(
          country.flag,
          height: 50,
        ),
        SizedBox(width: 20),
        Text(
          buildNumberFormat(country, double.parse(value)),
          style: buildTextStyle(isBold: true),
        ),
      ],
    );
  }
}
