# Coin Conversor | *_Flutter_*
## Description

A simple app built on **Flutter** to convert a **number values** into **currency formats**, such as **AKZ (Angola)**, **BRL (Brazil)** and **(USD) United States of America**.

![HomeScreen](assets/background/home-screen.png)
